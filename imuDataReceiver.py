from time import sleep
import codecs
import serial
import struct
import numpy as np
import math
from pyquaternion import Quaternion

class ImuDataReceiver:
    def __init__(self, port, baudRate, Simulation):
        self.ser = serial.Serial(port, baudRate, timeout=0)
        self.orientation = Quaternion()
        self.simulation = Simulation

    def getOrientation(self):
        return self.orientation

    def startDaq(self):
        buffer = b''
        while self.ser.is_open:
            if self.ser.in_waiting:
                buffer += self.ser.read(self.ser.in_waiting)
            while len(buffer) >=2:
                if buffer[0] == 83 and buffer[1] == 82 and buffer[18] == 83 and buffer[19] == 84:
                    break
                buffer = buffer[1:]
            if len(buffer) >= 20:
                #print(buffer[:20])
                self.parseData(buffer[:20])
                buffer = b''
            sleep(0.1)

    def parseData(self, buffer):
        quatTmp = struct.unpack('>4f', buffer[2:18])
        self.orientation = Quaternion(quatTmp[3], quatTmp[1], quatTmp[2], quatTmp[0])

        rx, ry, rz = self.quaternion_to_euler_angle(self.orientation.elements[0], self.orientation.elements[1], self.orientation.elements[2], self.orientation.elements[3])
        self.simulation().run(rx, ry, rz)

    def quaternion_to_euler_angle(self, w, x, y, z):
        ysqr = y * y
        t0 = +2.0 * (w * x + y * z)
        t1 = +1.0 - 2.0 * (x * x + ysqr)
        X = math.degrees(math.atan2(t0, t1))

        t2 = +2.0 * (w * y - z * x)
        t2 = +1.0 if t2 > +1.0 else t2
        t2 = -1.0 if t2 < -1.0 else t2
        Y = math.degrees(math.asin(t2))

        t3 = +2.0 * (w * z + x * y)
        t4 = +1.0 - 2.0 * (ysqr + z * z)
        Z = math.degrees(math.atan2(t3, t4))

        return X, Y, Z
        
            
