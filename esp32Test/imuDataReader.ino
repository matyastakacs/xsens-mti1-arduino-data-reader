#include <Wire.h>

#include <xbusparser.h>
#include <xbusutility.h>
#include <xbusdef.h>
#include <xbusmessage.h>
#include <xsdeviceid.h>

// Define I2C pins
#define SDA 21
#define SCL 22
#define DRDY 19

// IMU I2C slave address
byte address = 0x6B;

byte error;

byte buff[4];

bool isDataRdy;

union ArrayToInt32 {
  byte array[4];
 uint32_t integer;
};

union ArrayToFloat {
   byte array[4];
   float fval;
};

typedef union
{
 float number;
 uint8_t bytes[4];
} FLOATUNION_t;

// IMU data structure
struct IMUData {
  uint16_t packetCounter;
  uint32_t sampleTime;
  uint32_t statusWorld;
  float quaternion[4];
  float deltaQ[4];
  float deltaV[3];
  float acc[3];
  float rateTurn[3];
  float magField[3];
};

struct Opcode {
  byte ProtcollInfo = 0x01;
  byte ConfigureProtocol = 0x02;
  byte ControlPipe = 0x03; // Reduced Xbus data format
  byte PipeStatus = 0x4;
  byte NotificationPipe = 0x05; // Reduced Xbus data format
  byte MeasurementPipe = 0x06; // Reduced Xbus data format
 
 };


// ProtocollInfo message
struct MtsspInfo {
  uint8_t m_version;
  uint8_t m_drdyConfig;
};

// PipeStatus
struct MtsspConfiguration
{
  uint16_t m_notificationMessageSize;
  uint16_t m_measurementMessageSize;
};

IMUData imuData;

uint8_t quatRawData[16];

void setup() {
  
  pinMode(DRDY, INPUT);

  isDataRdy = false;

  delay(1000);

  // Open serial port
  Serial.begin(115200);

  // Start I2C port
  Wire.begin(SDA, SCL);

  isDataRdy = false;
  
  readPipeStatus();

  Serial.println("setup done");




}

// Read from the imu
void readFromIMU(byte address, byte opcode){

  //Start communication with imu
  Wire.beginTransmission(address);

  // Send the Opcode to the slave
  Wire.write(opcode);

  if(error = Wire.endTransmission() != 0) {
    Serial.print("Can not access the slave device after begin transmission. ErrorCode: ");
    Serial.println(error);
  }

  
}

// Read the pipe status
void readPipeStatus() {
  
    readFromIMU(address, 0x04);
    Wire.requestFrom(address, 4);
  
    int i = 0;
    while (Wire.available()){    
      buff[i] = Wire.read();
      i++;
    }

    // Initialize pipeStatus struct
    MtsspConfiguration pipeStatus;
    
    pipeStatus.m_notificationMessageSize = readU16(0, buff, true);
    pipeStatus.m_measurementMessageSize = readU16(2, buff, true);

    if(pipeStatus.m_notificationMessageSize) {
        readData(address, 0x05, pipeStatus.m_notificationMessageSize);
    }

    if(pipeStatus.m_measurementMessageSize) {
      readData(address, 0x06, pipeStatus.m_measurementMessageSize);
    }

  }

// Read data
void readData(byte address, byte pipe, uint16_t dataLength) {
    readFromIMU(address, pipe);
    Wire.requestFrom(address, dataLength);

    uint16_t preambleLength = 2;

    uint8_t buff[dataLength];
    uint8_t buff2[dataLength + preambleLength];
    uint16_t len = dataLength+preambleLength;
    buff2[0] = XBUS_PREAMBLE;
    buff2[1] = XBUS_MASTERDEVICE;
    int i = 0;
    while (Wire.available()){    
      buff[i] = Wire.read();
      buff2[i+2] = buff[i];
      i++;
    }

    uint8_t* buffPtr = (uint8_t*)buff;
    for(int i = 0; i < len; i++) {
    }

    // Measurement data
    if(buffPtr[0] == 54) {
      parseImuData(buff2, len);
    }

}

// Read uint8_t from data byte
uint8_t readU8(uint8_t startIdx, byte data[])
{
  return (uint8_t) data[startIdx];  
}

// Read uint16_t from data byte
uint16_t readU16(uint8_t startIdx, byte data[], bool lsb)
{
  if(lsb)
  {
    return ((uint16_t) data[startIdx] | (uint16_t) data[startIdx + 1] << 8);
  }
  else
  {
    return ((uint16_t) data[startIdx + 1] | (uint16_t) data[startIdx] << 8);  
  }
    
}

// Read uint32_t from data byte
uint32_t readU32(uint8_t startIdx, byte data[], bool lsb)
{
  ArrayToInt32 converter;

  if(lsb) {
    startIdx = startIdx + 3;
    for(int i = 0; i < 4; i++)
    {
      converter.array[i] = data[startIdx - i];
    }
  }
    else {
      for(int i = 0; i < 4; i++)
      {
        converter.array[i] = data[startIdx + i];
      }
    }

  return converter.integer;
}

// Convert 4bytes to float
float readFloat(uint8_t startIdx, byte data[], bool lsb)
{
  ArrayToFloat a2f;
  
  if(lsb)
  {
    startIdx = startIdx + 3;
    for(int i = 0; i < 4; i++)
    {
      a2f.array[i] = data[startIdx - i];
    }  
  }
  else
  {
    for(int i = 0; i < 4; i++)
    {
      a2f.array[i] = data[startIdx + i];
    }
  }

  return a2f.fval;
}

// Return the start idx of the given data id
uint8_t findDataId(enum XsDataIdentifier id, byte data[], uint8_t dataLength)
{
  uint8_t i = 0;
  uint16_t currentId = 0;
  while(i < dataLength - 2 && currentId != (uint16_t)id)
  {
    currentId = readU16(i, data, false);
    //Serial.println(currentId);
    i++;
  }
  if(i < dataLength - 2)
  {
    return i - 1;
  }
  else
  {
    return 0;  
  }

}

// Parse the buffer into the IMUData struct
void parseImuData(byte buff[], uint16_t dataLength) {

  uint8_t i = 0;
  uint8_t idx = 0;
  uint16_t currentId = 0;

  while(i < dataLength - 2) {
    currentId = readU16(i, buff, false);  
    switch (currentId) {

      case XDI_PacketCounter:
        imuData.packetCounter = readU16(i + 2, buff, false);
        break;

      case XDI_SampleTimeFine:
        imuData.sampleTime = readU32(i + 2, buff, false);
        break;

      case XDI_StatusWord:
        imuData.statusWorld = readU32(i + 2, buff, false);
        break;

      case XDI_Quaternion:
        idx = i + 3;
        setQuatRawData(idx, buff);
        for(int j = 0; j < 4; j++) {
          imuData.quaternion[j] = readFloat(idx, buff, true);
          idx = idx + 4;
        }
        break;

      case XDI_DeltaQ:
        idx = i + 2;
        for(int j = 0; j < 4; j++) {
          imuData.deltaQ[j] = readFloat(idx, buff, true);
          idx = idx + 4;
        }
        break;

      case XDI_DeltaV:
        idx = i + 2;
        for(int j = 0; j < 3; j++) {
          imuData.deltaV[j] = readFloat(idx, buff, true);
          idx = idx + 4;
        }
        break;

        case XDI_Acceleration:
        idx = i + 2;
        for(int j = 0; j < 3; j++) {
          imuData.acc[j] = readFloat(idx, buff, true);
          idx = idx + 4;
        }
        break;

        case XDI_RateOfTurn:
        idx = i + 2;
        for(int j = 0; j < 3; j++) {
          imuData.rateTurn[j] = readFloat(idx, buff, true);
          idx = idx + 4;
        }
        break;

        case XDI_MagneticField:
        idx = i + 2;
        for(int j = 0; j < 3; j++) {
          imuData.magField[j] = readFloat(idx, buff, true);
          idx = idx + 4;
        }
        break;
    }
    i++;
  }
}

// Put the quaternion raw data to the quatRawData array from the buffer
void setQuatRawData(uint8_t startIdx, byte buff[]) {
  for(int i = 0; i < 16; i++) {
    quatRawData[i] = buff[startIdx + i];  
  }
}


// Write the quaternion raw data to the serial port
void writeQuatRawDataToSerial() {
  Serial.write('S');
  Serial.write('R');

  for(int i = 0; i < sizeof(quatRawData); i++) {
    Serial.write(quatRawData[i]);  
  }

  Serial.write('S');
  Serial.write('T');
}


void loop() {

/*
  while(!isDataRdy) {
    isDataRdy = digitalRead(DRDY);
    //Serial.println(digitalRead(DRDY));

  }*/ 

  isDataRdy = false;
  readPipeStatus();
  writeQuatRawDataToSerial();

  delay(10);
  


}
